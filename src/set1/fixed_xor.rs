use utils::conversions::from_hex::{HexConversionError, FromHex};
use utils::conversions::to_hex::ToHex;

// TODO(Dan) make this implement error
#[derive(Debug)]
pub enum FixedXORError {
    LengthsNotEqual(usize, usize),
    InvalidHex(HexConversionError),
}

pub fn fixed_xor_str(left: &str, right: &str) -> Result<String, FixedXORError> {
    left.from_hex()
        .and_then(|left_buf| right.from_hex().map(|right_buf| (left_buf, right_buf)))
        .map_err(FixedXORError::InvalidHex)
        .and_then(|(l, r)| fixed_xor(&l, &r))
        .map(|bytes| bytes.to_hex())
}

pub fn fixed_xor(left: &[u8], right: &[u8]) -> Result<Vec<u8>, FixedXORError> {
    if left.len() != right.len() {
        Err(FixedXORError::LengthsNotEqual(left.len(), right.len()))
    } else {
        Ok(left.into_iter().zip(right.into_iter()).map(|(l, r)| l ^ r).collect())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fixed_xor_str_test() {
        assert_eq!(fixed_xor_str("1c0111001f010100061a024b53535009181c",
                                 "686974207468652062756c6c277320657965")
                       .unwrap(),
                   "746865206b696420646f6e277420706c6179");
    }
}
