use std::string::FromUtf8Error;
use utils::conversions::to_base64::ToBase64;
use utils::conversions::from_hex::{HexConversionError, FromHex};

#[derive(Debug)]
pub enum HexToBase64Error {
    InvalidHex(HexConversionError),
    InvalidUtf8(FromUtf8Error),
}

pub fn hex_to_base64(input: &str) -> Result<String, HexToBase64Error> {
    match input.from_hex() {
        Err(e) => Err(HexToBase64Error::InvalidHex(e)),
        Ok(bytes) => {
            match bytes.to_base64() {
                Ok(s) => Ok(s),
                Err(e) => Err(HexToBase64Error::InvalidUtf8(e)),
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cryptopals_case() {
        assert_eq!(hex_to_base64("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d").unwrap(),
                   "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t");
    }


}
