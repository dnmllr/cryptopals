use std::fmt;
use std::error;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum HexConversionError {
    UnknownCharacter(u8),
    UnknownCharacterAt(u8, usize),
    InvalidHexLength,
}

impl fmt::Display for HexConversionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            HexConversionError::UnknownCharacter(b) => write!(f, "Byte out of range {}", b),
            HexConversionError::UnknownCharacterAt(b, i) => {
                write!(f, "Byte out of range {} at index {}", b, i)
            }
            HexConversionError::InvalidHexLength => {
                write!(f,
                       "Invalid Hex Length, number of bytes should be divisible by two")
            }
        }
    }
}

impl error::Error for HexConversionError {
    fn description(&self) -> &str {
        match *self {
            HexConversionError::UnknownCharacter(..) |
            HexConversionError::UnknownCharacterAt(..) => "invalid character",
            HexConversionError::InvalidHexLength => "Invalid length of hex characters",
        }
    }
}


pub trait FromHex {
    fn from_hex(&self) -> Result<Vec<u8>, HexConversionError>;
}

fn convert_byte(byte: u8) -> Result<u8, HexConversionError> {
    match byte {
        b'A'...b'F' => Ok(byte - b'A' + 10),
        b'a'...b'f' => Ok(byte - b'a' + 10),
        b'0'...b'9' => Ok(byte - b'0'),
        _ => Err(HexConversionError::UnknownCharacter(byte)),
    }
}

impl FromHex for str {
    fn from_hex(&self) -> Result<Vec<u8>, HexConversionError> {
        if (self.len() & 1) == 1 {
            Err(HexConversionError::InvalidHexLength)
        } else {
            let mut buffer = Vec::with_capacity(self.len() / 2);
            for (i, bytes) in self.as_bytes().chunks(2).enumerate() {
                match convert_byte(bytes[0]) {
                    Err(HexConversionError::UnknownCharacter(byte)) => {
                        return Err(HexConversionError::UnknownCharacterAt(byte, i * 2))
                    }
                    Ok(left) => {
                        match convert_byte(bytes[1]) {
                            Err(HexConversionError::UnknownCharacter(byte)) => {
                                return Err(HexConversionError::UnknownCharacterAt(byte, i * 2 + 1))
                            }
                            Ok(right) => buffer.push((left << 4) | right),
                            _ => unreachable!(),
                        }
                    }
                    _ => unreachable!(),
                }
            }
            Ok(buffer)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_from_hex() {
        assert_eq!(&"666f6f626172".from_hex().unwrap(), b"foobar");
    }
}
