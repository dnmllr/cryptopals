pub trait ToHex {
    fn to_hex(&self) -> String;
}

const CHARS: &'static [u8] = b"0123456789abcdef";

impl ToHex for [u8] {
    fn to_hex(&self) -> String {
        let mut s = String::with_capacity(self.len() * 2);
        for byte in self.iter() {
            s.push(CHARS[(byte >> 4) as usize] as char);
            s.push(CHARS[(byte & 0xf) as usize] as char);
        }
        s
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn to_hex() {
        assert_eq!("foobar".as_bytes().to_hex(), "666f6f626172");
    }
}
