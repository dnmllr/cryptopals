use std::string::FromUtf8Error;

pub trait ToBase64 {
    fn to_base64(&self) -> Result<String, FromUtf8Error>;
}

const CHARS: &'static [u8] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

impl ToBase64 for [u8] {
    fn to_base64(&self) -> Result<String, FromUtf8Error> {
        let mut output_bytes: Vec<u8> = vec![b'='; 4 * ((2 + self.len()) / 3)];
        for (input, output) in self.chunks(3).zip(output_bytes.chunks_mut(4)) {
            match *input {
                [a, b, c] => output.copy_from_slice(&three_bytes_to_base64(a, b, c)),
                [a, b] => output.copy_from_slice(&two_bytes_to_base64(a, b)),
                [a] => output.copy_from_slice(&one_byte_to_base64(a)),
                _ => {}
            }
        }
        String::from_utf8(output_bytes)
    }
}

fn three_bytes_to_base64(a: u8, b: u8, c: u8) -> [u8; 4] {
    [CHARS[(a >> 2) as usize],
     CHARS[(((a & 0x03) << 4) | (b >> 4)) as usize],
     CHARS[(((b & 0x0f) << 2) | (c >> 6)) as usize],
     CHARS[(c & 0x3f) as usize]]
}

fn two_bytes_to_base64(a: u8, b: u8) -> [u8; 3] {
    [CHARS[(a >> 2) as usize],
     CHARS[(((a & 0x03) << 4) | (b >> 4)) as usize],
     CHARS[((b & 0x0f) << 2) as usize]]
}

fn one_byte_to_base64(a: u8) -> [u8; 2] {
    [CHARS[(a >> 2) as usize], CHARS[((a & 0x03) << 4) as usize]]
}
